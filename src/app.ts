import express from "express";
// import http from "http";

const app = express();

app.set("port", 3000);
app.use(express.static(__dirname + "public"));
app.set("views", __dirname + "/public");
app.use("/js/", express.static(__dirname + "/public/js"));

app.engine("html", require("ejs").renderFile);
app.set("view engine", "html");
app.get("/", function(req, res) {
	res.render("index.html");
});

export default app;
